//
//  TipsView.swift
//  DatasSwiftApp
//
//  Created by Aleksandr Afanasiev on 04.09.17.
//  Copyright © 2017 Aleksandr Afanasiev. All rights reserved.
//

import UIKit

class TipsView: UIView {

    let tipsLabel = UILabel()
    let tipsTextView = UITextView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func commonInit() {
        
        tipsLabel.frame = CGRect(x: 10, y: 20, width: self.frame.width - 20, height: 25)
        tipsLabel.textAlignment = .center
        tipsLabel.font = UIFont.boldSystemFont(ofSize: 24)
        tipsLabel.textColor = UIColor.black
        self.addSubview(tipsLabel)
        
        tipsTextView.frame = CGRect(x: 20, y: 55, width: self.frame.width - 40, height: self.frame.height - 45)
        tipsTextView.textAlignment = .center
        tipsTextView.font = UIFont.systemFont(ofSize: 18)
        self.addSubview(tipsTextView)
        
    }
    
}
