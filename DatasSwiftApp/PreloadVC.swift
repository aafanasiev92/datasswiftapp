//
//  PreloadVC.swift
//  DatasSwiftApp
//
//  Created by Aleksandr Afanasiev on 04.09.17.
//  Copyright © 2017 Aleksandr Afanasiev. All rights reserved.
//

import UIKit

class PreloadVC: UIViewController {

    @IBOutlet weak var tipsScrollView: UIScrollView!
    @IBOutlet weak var itemIndicator1: UIView!
    @IBOutlet weak var itemIndicator2: UIView!
    @IBOutlet weak var itemIndicator3: UIView!

    @IBOutlet weak var preview: UIView!
    @IBOutlet weak var viewLoading: UIView!
    @IBOutlet weak var loginView: UIView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var phoneTextField: UITextField!
    
    
    let greenColor = UIColor(red: 26/255, green: 159/255, blue: 40/255, alpha: 1)
    let grayColor = UIColor.lightGray
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupScrollView()
        
        phoneTextField.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @IBAction func continueAction(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.2, animations: {
            self.preview.isHidden = true
            self.viewLoading.isHidden = false
            self.activityIndicator.startAnimating()
        }) { (_) in
            self.durationOfAnimation(seconds: 4)
        }
    }
    @IBAction func loginAction(_ sender: UIButton) {
        self.hideKeyboard()
    }
    
    func durationOfAnimation(seconds: Double) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true

            UIView.animate(withDuration: 0.2, animations: {
                self.viewLoading.isHidden = true
                self.loginView.isHidden = false
            })
            
        }
    }
    
    func setupScrollView() {
        self.tipsScrollView.contentSize = CGSize(width: self.view.frame.width * 3, height: self.tipsScrollView.frame.height)
        self.tipsScrollView.delegate = self
        
        for iterator in 0..<3 {
            let tipView = TipsView(frame: CGRect(x: self.view.frame.width * CGFloat(iterator), y: 0, width: self.view.frame.width, height: self.tipsScrollView.frame.height))
            tipView.tipsLabel.text = "Текст типсы \(iterator + 1)"
            tipView.tipsTextView.text = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. "
            self.tipsScrollView.addSubview(tipView)
        }
    }
    
    func hideKeyboard() {
        view.endEditing(true)
    }
    

}

extension PreloadVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let position = Int(scrollView.contentOffset.x / self.view.frame.width)
        
        switch position {
        case 0:
            itemIndicator1.backgroundColor = greenColor
            itemIndicator2.backgroundColor = grayColor
            itemIndicator3.backgroundColor = grayColor
        case 1:
            itemIndicator1.backgroundColor = grayColor
            itemIndicator2.backgroundColor = greenColor
            itemIndicator3.backgroundColor = grayColor
        default:
            itemIndicator1.backgroundColor = grayColor
            itemIndicator2.backgroundColor = grayColor
            itemIndicator3.backgroundColor = greenColor
        }
    }
}

extension PreloadVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == phoneTextField) {
            
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let components = (newString as NSString).components(separatedBy: NSCharacterSet.decimalDigits.inverted)
            let decimalString = components.joined(separator: "") as NSString
            let length = decimalString.length
            let hasLeadingOne = length > 0 && decimalString.character(at: 0) == (1 as unichar)
            
            if length == 0 || (length > 12 && !hasLeadingOne) || length > 13 {
                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
                return (newLength > 12) ? false : true
            }
            
            var index = 0 as Int
            let formattedString = NSMutableString()
            
            if hasLeadingOne {
                formattedString.append("1 ")
                index += 1
            }
            
            if length - index > 10 {
                let rangeCode = length - 10
                let code = decimalString.substring(with: NSMakeRange(index, rangeCode))
                formattedString.appendFormat("+%@", code)
                index += rangeCode
            }
            
            if (length - index) > 7 {
                let rangeCode = length - 7 > 3 ? 3 : length - 7
                let areaCode = decimalString.substring(with: NSMakeRange(index, rangeCode))
                formattedString.appendFormat("(%@)", areaCode)
                index += rangeCode
            }
            
            if length - index > 3 {
                let prefix = decimalString.substring(with: NSMakeRange(index, 3))
                formattedString.appendFormat("%@-", prefix)
                index += 3
            }
            
            let remainder = decimalString.substring(from: index)
            formattedString.append(remainder)
            textField.text = formattedString as String
            
            return false
        } else {
            return true
        }
    }
}

